<?php

namespace App\Controller;

use App\Entity\Docteur;
use App\Form\DocteurType;
use App\Repository\DocteurRepository;
use App\Repository\RdvRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class MedecinController extends AbstractController
{
    /**
     * @Route("/medecin", name="medecin")
     */
    public function index(DocteurRepository $docteurRepository, RdvRepository $rdvRepository, Request $request): Response
    {
        $docteurs = $docteurRepository->findAll();
        $nbrdvs = $rdvRepository->findNbRdvInCurrentMonth();
     //   $test  =  $rdvRepository->findNbRdvInCurrentMonth2();
     //   dump($test);
     //  dd($nbrdvs); // dump and die
        $session = $request->getSession();
        $medecinlast = $session->get("medecin-last");
        //dd($medecinlast);
        return $this->render('medecin/index.html.twig', [
            'docteurs' => $docteurs
        ]);
    }

    /**
     * @Route("/medecin/{id<\d+>}", name="medecin_view")
     */
    public function view(int $id , DocteurRepository $docteurRepository, Request $request): Response
    {
        $docteur = $docteurRepository->find($id);

        $session = $request->getSession();
        $session->set('medecin-last' , $id);

        return $this->render('medecin/view.html.twig', [
            'docteur' => $docteur
        ]);
    }

    /**
     * @Route("/medecinAdd", name="medecin_add")
     * @IsGranted("ROLE_MEDECIN")
     */
    public function add(ManagerRegistry $doctrine, Request $request): Response
    {
        if ($this->getUser()->getDocteur() != null) {
            // la fiche docteur existe déjà
            $docteur =    $this->getUser()->getDocteur();
        } else {
            $docteur = new Docteur();
        }
        $form = $this->createForm(DocteurType::class, $docteur);

        $manager = $doctrine->getManager();

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $docteur = $form->getData();
            $docteur->setCompteUtilisateur($this->getUser());

            $manager->persist($docteur);
            $manager->flush();
           return  $this->redirectToRoute("medecin_view",array("id" => $docteur->getId()));
        }


        return $this->renderForm('medecin/add.html.twig', [
            'form' => $form
        ]);
    }

    /**
     * @Route("/medecinRdv", name="medecin_rdv")
     * @IsGranted("ROLE_MEDECIN")
     */
    public function medecinrdv( RdvRepository $rdvRepository): Response
    {
        $rdvsDuJour =  $rdvRepository->findRdvDuJourForOneDocteur($this->getUser()->getDocteur());
        dd($rdvsDuJour);
        return $this->render('medecin/rdvs.html.twig' , []);
    }


}
