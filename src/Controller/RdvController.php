<?php

namespace App\Controller;

use App\Form\DocteurType;
use App\Entity\Rdv;
use App\Form\RdvType;
use App\Repository\TypeConsultationRepository;
use App\Repository\RdvRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class RdvController extends AbstractController
{
    /**
     * @Route("/rdv", name="app_rdv")
     * @IsGranted("ROLE_USER")
     */
    public function index(TypeConsultationRepository $typeConsultationRepository): Response
    {
        $typesConsult = $typeConsultationRepository->findAll();
        return $this->render('rdv/index.html.twig', [
            'types' => $typesConsult,
        ]);
    }

    /**
     * @Route("/mesrdv", name="app_mesrdv")
     * @IsGranted("ROLE_USER")
     */
    public function mesrdv(RdvRepository $rdvRepository): Response
    {
        $rdvs = $rdvRepository->findBy(['patient' => $this->getUser()] , ['creneau' => 'DESC']);
        return $this->render('rdv/mesrdv.html.twig', [
            'rdvs' => $rdvs, 'now' => new \DateTime()
        ]);
    }

    /**
     * @Route("/addrdv/{idTypeConsult}", name="app_addrdv")
     * @IsGranted("ROLE_USER")
     */
    public function add($idTypeConsult, TypeConsultationRepository $typeConsultationRepository, ManagerRegistry $doctrine, Request $request): Response
    {
        $rdv = new Rdv();
        $typeConsult = $typeConsultationRepository->find($idTypeConsult);
        $form = $this->createForm(RdvType::class, $rdv , array('medecins' => $typeConsult->getMedecins()));


        $manager = $doctrine->getManager();

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $rdv = $form->getData();
            // on récupère l'utilisateur connecté !
            $rdv->setPatient($this->getUser());
            // on récupère de typeConsultation
            $rdv->setTypeConsultation($typeConsult);
            $manager->persist($rdv);
            $manager->flush();
            return  $this->redirectToRoute("app_rdv");
        }
        return $this->renderForm('rdv/add.html.twig', [
            'form' => $form
        ]);
    }



    /**
     * @Route("/removerdv/{id}", name="app_removerdv")
     * @IsGranted("ROLE_USER")
     */
    public function remove($id, RdvRepository $rdvRepository, ManagerRegistry $doctrine): Response
    {
        $rdv  = $rdvRepository->find($id);
        $manager = $doctrine->getManager();
        $manager->remove($rdv);
        $manager->flush();
        return $this->redirectToRoute("app_mesrdv")  ;
    }
}
