<?php

namespace App\DataFixtures;

use App\Entity\Docteur;
use App\Entity\TypeConsultation;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    private UserPasswordHasherInterface $hasher;

    public function __construct(UserPasswordHasherInterface $hasher) {
        $this->hasher = $hasher;
    }

    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);

        $user = new User();
        $user->setEmail('gillard.cecile@ltpdampierre.fr');
        $pass = $this->hasher->hashPassword($user, 'cecile');
        $user->setPassword($pass);
        $user->setNom("Gillard");
        $user->setPrenom("Cécile");
        $user->setTelephone("012345859632");
        $manager->persist($user);

        $doc = new Docteur();
        $doc->setNom("Dr Gillard");
        $doc->setTelephone("025442658");
        $doc->setVille("VA");
        $doc->setDescription("Bienvenue sur mon profil");
        $doc->setCompteUtilisateur($user);
        $manager->persist($doc);

        $typeConsults = ['Medecine Générale', 'Psy', 'Kiné', 'Pédiatre'];
        foreach ($typeConsults as $t) {
            $type = new TypeConsultation();
            $type->setLibelle($t);
            $type->setDuree(mt_rand(10,30));
            $manager->persist($type);
        }


        $manager->flush();
    }
}
