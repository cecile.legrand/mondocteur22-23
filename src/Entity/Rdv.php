<?php

namespace App\Entity;

use App\Repository\RdvRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RdvRepository::class)
 */
class Rdv
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $creneau;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="rdvs")
     */
    private $patient;

    /**
     * @ORM\ManyToOne(targetEntity=TypeConsultation::class, inversedBy="rdvs")
     */
    private $type_consultation;

    /**
     * @ORM\ManyToOne(targetEntity=Docteur::class, inversedBy="rdvs")
     */
    private $medecin;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreneau(): ?\DateTimeInterface
    {
        return $this->creneau;
    }

    public function setCreneau(\DateTimeInterface $creneau): self
    {
        $this->creneau = $creneau;

        return $this;
    }

    public function getPatient(): ?User
    {
        return $this->patient;
    }

    public function setPatient(?User $patient): self
    {
        $this->patient = $patient;

        return $this;
    }

    public function getTypeConsultation(): ?TypeConsultation
    {
        return $this->type_consultation;
    }

    public function setTypeConsultation(?TypeConsultation $type_consultation): self
    {
        $this->type_consultation = $type_consultation;

        return $this;
    }

    public function getMedecin(): ?Docteur
    {
        return $this->medecin;
    }

    public function setMedecin(?Docteur $medecin): self
    {
        $this->medecin = $medecin;

        return $this;
    }
}
