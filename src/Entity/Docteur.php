<?php

namespace App\Entity;

use App\Repository\DocteurRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DocteurRepository::class)
 */
class Docteur
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ville;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $telephone;

    /**
     * @ORM\OneToMany(targetEntity=Horaires::class, mappedBy="medecin", orphanRemoval=true)
     */
    private $horaires;

    /**
     * @ORM\ManyToMany(targetEntity=TypeConsultation::class, mappedBy="medecins")
     */
    private $typeConsultations;

    /**
     * @ORM\OneToMany(targetEntity=Rdv::class, mappedBy="medecin")
     */
    private $rdvs;

    /**
     * @ORM\OneToOne(targetEntity=User::class, inversedBy="docteur", cascade={"persist", "remove"})
     */
    private $compteUtilisateur;

    public function __construct()
    {
        $this->horaires = new ArrayCollection();
        $this->typeConsultations = new ArrayCollection();
        $this->rdvs = new ArrayCollection();
    }

    public function __toString() {
        return $this->id.' - '.$this->getNom();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * @return Collection<int, Horaires>
     */
    public function getHoraires(): Collection
    {
        return $this->horaires;
    }

    public function addHoraire(Horaires $horaire): self
    {
        if (!$this->horaires->contains($horaire)) {
            $this->horaires[] = $horaire;
            $horaire->setMedecin($this);
        }

        return $this;
    }

    public function removeHoraire(Horaires $horaire): self
    {
        if ($this->horaires->removeElement($horaire)) {
            // set the owning side to null (unless already changed)
            if ($horaire->getMedecin() === $this) {
                $horaire->setMedecin(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, TypeConsultation>
     */
    public function getTypeConsultations(): Collection
    {
        return $this->typeConsultations;
    }

    public function addTypeConsultation(TypeConsultation $typeConsultation): self
    {
        if (!$this->typeConsultations->contains($typeConsultation)) {
            $this->typeConsultations[] = $typeConsultation;
            $typeConsultation->addMedecin($this);
        }

        return $this;
    }

    public function removeTypeConsultation(TypeConsultation $typeConsultation): self
    {
        if ($this->typeConsultations->removeElement($typeConsultation)) {
            $typeConsultation->removeMedecin($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Rdv>
     */
    public function getRdvs(): Collection
    {
        return $this->rdvs;
    }

    public function addRdv(Rdv $rdv): self
    {
        if (!$this->rdvs->contains($rdv)) {
            $this->rdvs[] = $rdv;
            $rdv->setMedecin($this);
        }

        return $this;
    }

    public function removeRdv(Rdv $rdv): self
    {
        if ($this->rdvs->removeElement($rdv)) {
            // set the owning side to null (unless already changed)
            if ($rdv->getMedecin() === $this) {
                $rdv->setMedecin(null);
            }
        }

        return $this;
    }

    public function getCompteUtilisateur(): ?User
    {
        return $this->compteUtilisateur;
    }

    public function setCompteUtilisateur(?User $compteUtilisateur): self
    {
        $this->compteUtilisateur = $compteUtilisateur;

        return $this;
    }
}
