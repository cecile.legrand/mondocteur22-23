<?php

namespace App\Form;

use App\Entity\Rdv;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\ChoiceList\ChoiceList;

class RdvType extends AbstractType
{
    private $medecins;

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $this->medecins = $options['medecins'];
        $builder
            ->add('creneau')
            ->add('medecin', ChoiceType::class, [ 'choices' => $this->medecins,
                'choice_label' =>  'nom'] )
            ->add('save', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Rdv::class,
            'medecins' => null
        ]);
    }
}
