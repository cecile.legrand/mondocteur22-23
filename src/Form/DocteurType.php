<?php

namespace App\Form;

use App\Entity\Docteur;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Regex;

class DocteurType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom', TextType::class, ["required" => true,
                                            "constraints" => [
                                                new Length(['min' => 3 , 'minMessage' => 'Le nom doit comporter 3 caractères minimum']),

                                            ]
            ])
            ->add('description')
            ->add('ville')
            ->add('telephone', TelType::class, [ "constraints" => [new Regex([ 'pattern' => "/^[0-9]{10}$/",
                                            "message" => "Votre numéro de téléphone doit être composé de 10 chiffre"]) ] ])
            ->add('Ajouter' , SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Docteur::class,
        ]);
    }
}
