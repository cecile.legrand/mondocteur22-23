<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230120125028 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE docteur ADD compte_utilisateur_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE docteur ADD CONSTRAINT FK_83A7A4393BE1373C FOREIGN KEY (compte_utilisateur_id) REFERENCES user (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_83A7A4393BE1373C ON docteur (compte_utilisateur_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE docteur DROP FOREIGN KEY FK_83A7A4393BE1373C');
        $this->addSql('DROP INDEX UNIQ_83A7A4393BE1373C ON docteur');
        $this->addSql('ALTER TABLE docteur DROP compte_utilisateur_id');
    }
}
